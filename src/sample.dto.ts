import {
  IsArray,
  IsDate,
  IsEmail,
  IsNumber,
  IsNumberString,
  IsOptional,
  IsString,
  Length,
  MinLength,
  Validate,
} from 'class-validator';
import { CustomValidator } from './custom-validator';
import { Exclude, Expose, Transform } from 'class-transformer';

export class SampleDto {
  @IsArray()
  @Transform(
    (value, obj, type) => {
      console.log('transform ===>');
      console.log('value =>', value);
      console.log('obj =>', obj);
      console.log('type =>', type);

      return [`aa${value}`];
    },
    { toClassOnly: false },
  )
  p1: boolean;

  // @IsString()
  // p1: string;

  @Expose()
  @IsString()
  @MinLength(4, {
    // groups: ['registration'],
  })
  p2: string;

  @IsEmail(
    {},
    {
      groups: ['another'],
    },
  )
  p3: string;

  p4: string;

  @Validate(CustomValidator, [1, 2])
  p6: string;

  @IsOptional()
  @IsDate()
  p7: Date;
}
