import {
  ArgumentsHost,
  BadRequestException,
  Catch,
  ExceptionFilter,
} from '@nestjs/common';

@Catch(BadRequestException)
export class ValidationExceptionFilter implements ExceptionFilter {
  catch(exception: BadRequestException, host: ArgumentsHost): any {
    const response = exception.getResponse();
    const status = exception.getStatus();

    console.log(response);
    console.log(status);

    host
      .switchToHttp()
      .getResponse()
      .send('hola');
  }
}
