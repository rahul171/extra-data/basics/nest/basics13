import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import {
  HttpStatus,
  Catch,
  HttpException,
  Body,
  Post,
  NestInterceptor,
  ValidationPipe,
} from '@nestjs/common';
import { ValidationExceptionFilter } from './validation-exception.filter';

// import { ValidationPipe } from './validation.pipe';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalFilters(new ValidationExceptionFilter());
  app.useGlobalPipes(
    new ValidationPipe({
      // skipMissingProperties: true,
      // whitelist: true,
      // forbidNonWhitelisted: true,
      // forbidUnknownValues: true,
      // disableErrorMessages: true,
      // errorHttpStatusCode: HttpStatus.FORBIDDEN,
      // dismissDefaultMessages: true,
      // validationError: {
      //   target: true,
      //   value: true,
      // },
      // exceptionFactory: errors => {
      //   console.log(errors);
      //   // console.log('--');
      //   // console.log(errors[0].children.map(item => item.children));
      //   // console.log('--');
      //   // console.log(
      //   //   errors[0].children.map(item =>
      //   //     item.children.map(item => item.constraints),
      //   //   ),
      //   // );
      //   // console.log('--');
      //   return new HttpException('hello', 400);
      // },
      // exceptionFactory: errors => {
      //   console.log('errors =>', errors);
      //   return new HttpException(
      //     {
      //       errors: (() => {
      //         const nErrors = [];
      //         for (const error of errors) {
      //           const values = Object.values(error.constraints);
      //           for (const value of values) {
      //             nErrors.push(value);
      //           }
      //         }
      //         return nErrors;
      //       })(),
      //     },
      //     300,
      //   );
      // },
      // skipMissingProperties: true,
      // transforms controller route params
      transform: true,
      transformOptions: {
        // transforms dto object's properties
        enableImplicitConversion: true,
      },
    }),
    // new ValidationPipe(),
  );
  await app.listen(3000);
}
bootstrap();
