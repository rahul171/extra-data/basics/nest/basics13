import { Injectable } from '@nestjs/common';
import { Sample1Dto } from './sample1.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Table1 } from './table1.entity';
import { Repository } from 'typeorm';

@Injectable()
export class AppService {
  constructor(
    @InjectRepository(Table1)
    private table1Repository: Repository<Table1>,
  ) {
    // const table1 = new Table1();
    // table1.p1 = new Date();
    // table1.p2 = new Date();
    // this.table1Repository.save(table1);
  }

  getHello(): string {
    return 'Hello World!';
  }

  saveData(data: Sample1Dto) {
    console.log(data);
    const table1 = new Table1();
    table1.p1 = data.p1;
    this.table1Repository.save(table1);
  }

  getData() {
    return this.table1Repository.find();
  }
}
