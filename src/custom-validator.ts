import {
  ValidationArguments,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';

@ValidatorConstraint({ name: 'someName', async: true })
export class CustomValidator implements ValidatorConstraintInterface {
  async validate(value: any, args: ValidationArguments) {
    // console.log('value =>', value);
    // console.log('typeof =>', typeof value);
    // console.log('args =>', args);
    // return !!(await new Promise((resolve, reject) => {
    //   setTimeout(() => {
    //     resolve(Math.floor(Math.random() * 2));
    //   }, 0);
    // }));
    return true;
  }

  defaultMessage(validationArguments?: ValidationArguments): string {
    // console.log('validationArguments =>', validationArguments);
    return 'error custom';
  }
}
