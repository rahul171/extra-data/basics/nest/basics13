import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Table1 {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('timestamp')
  p1: Date;

  @Column('datetime')
  p2: Date;

  // @CreateDateColumn()
  // createdAt: Date;
}
