import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Table1 } from './table1.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: 'abcd1234',
      database: 'nest-basics-typeorm1',
      entities: [Table1],
      synchronize: true,
    }),
    TypeOrmModule.forFeature([Table1]),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
