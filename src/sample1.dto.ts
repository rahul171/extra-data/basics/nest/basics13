import { IsDate } from 'class-validator';

export class Sample1Dto {
  @IsDate()
  p1: Date;
}
