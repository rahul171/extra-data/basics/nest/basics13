import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { AppService } from './app.service';
import { SampleDto } from './sample.dto';
import { Sample1Dto } from './sample1.dto';
import { Sample2Dto } from './sample2.dto';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Post()
  getHello(@Body() body: SampleDto) {
    console.log(body);
    console.log(typeof body);
    return body;
  }

  @Post('sample2')
  checkSample2(@Body() body: Sample2Dto) {
    console.log(body);
    console.log(typeof body);
    return body;
  }

  @Post(':id')
  getHello1(@Param('id') id: number) {
    console.log(id);
    return { id };
  }

  @Post('date')
  async saveData(@Body() body: Sample1Dto) {
    await this.appService.saveData(body);
    return 'thank you';
  }

  @Get('date')
  async getData() {
    return this.appService.getData();
  }
}
