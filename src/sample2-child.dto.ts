import { IsNumber, IsString } from 'class-validator';
export class Sample2ChildDto {
  @IsNumber()
  p21: number;

  @IsString()
  p22: string;
}
