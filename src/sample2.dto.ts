import {
  ArrayNotEmpty,
  IsArray,
  IsDefined,
  IsNumber,
  IsOptional,
  IsString,
  ValidateNested,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { plainToClass, Transform, Type } from 'class-transformer';
import { Sample2ChildDto } from './sample2-child.dto';

import {
  registerDecorator,
  ValidationOptions,
  ValidationArguments,
} from 'class-validator';

interface Abcd {
  p21: number;
}

@ValidatorConstraint({ name: 'isNonPrimitiveArray', async: false })
export class IsNonPrimitiveArrayConstraint
  implements ValidatorConstraintInterface {
  validate(value: any, args: ValidationArguments): boolean {
    return (
      Array.isArray(value) &&
      value.reduce(
        (a, b) => a && typeof b === 'object' && !Array.isArray(b),
        true,
      )
    );
  }

  defaultMessage(args: ValidationArguments): string {
    return `${args.property} must be a non primitive array conforming to the specified constraints`;
  }
}

export function IsNonPrimitiveArray(validationOptions?: ValidationOptions) {
  return (object: any, propertyName: string) => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      constraints: [],
      options: validationOptions,
      validator: IsNonPrimitiveArrayConstraint,
    });
  };
}

@ValidatorConstraint({ name: 'isNonPrimitive', async: false })
export class IsNonPrimitiveConstraint implements ValidatorConstraintInterface {
  validate(value: any, args: ValidationArguments): boolean {
    return typeof value === 'object' && !Array.isArray(value);
  }

  defaultMessage(args: ValidationArguments): string {
    return `${args.property} must be a non primitive conforming to the specified constraints`;
  }
}

export function IsNonPrimitive(validationOptions?: ValidationOptions) {
  return (object: any, propertyName: string) => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      constraints: [],
      options: validationOptions,
      validator: IsNonPrimitiveConstraint,
    });
  };
}

class Sample2Child1Child1Child1Dto {
  @IsNumber()
  p3331: number;

  @IsString()
  p3332: string;
}

class Sample2Child1Child1Dto {
  @IsNumber()
  p331: number;

  @IsString()
  p332: string;

  @ValidateNested({ each: true })
  @Type(() => Sample2Child1Child1Child1Dto)
  @IsNonPrimitiveArray()
  @ArrayNotEmpty()
  p333: Sample2Child1Child1Child1Dto[];
}

class Sample2Child1Dto {
  @IsNumber()
  p31: number;

  @IsString()
  p32: string;

  @ValidateNested({ each: true })
  @Type(() => Sample2Child1Child1Dto)
  @IsNonPrimitiveArray()
  @ArrayNotEmpty()
  p33: Sample2Child1Child1Dto[];
}

export class Sample2Dto {
  // @IsString()
  @IsNumber()
  // @Transform(
  //   value => {
  //     console.log(value);
  //     console.log(typeof value);
  //     console.log('----');
  //     return value;
  //   },
  //   { toClassOnly: true },
  // )
  // p1: string;
  p1: number;

  @ValidateNested()
  @Type(type => {
    // console.log('type =>', type);
    return Sample2ChildDto;
  })
  // @Transform(
  //   value => {
  //     console.log('p2 =>', value);
  //     return value;
  //   },
  //   { toClassOnly: true },
  // )
  // changing the type here won't do anything even if enableImplicitConversion = true
  // p2: Abcd;
  // p2: number;
  @IsNonPrimitive()
  p2: Sample2ChildDto;

  // @ValidateNested()
  @ValidateNested({ each: true })
  @Type(() => Sample2Child1Dto)
  @IsNonPrimitiveArray()
  @ArrayNotEmpty()
  p3: Sample2Child1Dto[];
}
