import {
  ArgumentMetadata,
  HttpException,
  Injectable,
  PipeTransform,
} from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { validate } from 'class-validator';
import { SampleDto } from './sample.dto';

@Injectable()
export class ValidationPipe implements PipeTransform {
  async transform(value: any, { metatype }: ArgumentMetadata) {
    if (!value) {
      throw new HttpException('Request parameters missing', 400);
    }

    if (!metatype || !this.toValidate(metatype)) {
      return value;
    }

    const object = plainToClass(metatype, value);
    // console.log('metatype =>', metatype);
    // console.log('direct class =>', SampleDto);
    // console.log('typeof metatype =>', typeof metatype);
    // console.log('object =>', object);

    // if the dto class doesn't have any class-validator decorator, then
    // forbidUnknownValues will throw an error. It checks if the object is
    // a validator object or not, It doesn't have to do anything with
    // the unknown properties of the dto object.
    const errors = await validate(object, {
      forbidUnknownValues: true,
      validationError: {
        target: true,
        value: true,
      },
    });

    // console.log(value);
    console.log(errors);

    if (errors.length > 0) {
      throw new HttpException('Validation error', 400);
    }

    return value;
  }

  private toValidate(metatype: Function): boolean {
    const types: Function[] = [Number, String, Array, Object, Boolean];
    return !types.includes(metatype);
  }
}
